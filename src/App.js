// import "./App.css";
// import AppNavbar from "./components/AppNavbar";
// import Home from "./pages/Home";
// import Courses from "./pages/Courses";
// import Register from "./pages/Register";
// import Login from "./pages/Login";
// import Logout from "./pages/Logout";
// import PageNotFound from "./pages/ErrorPage";
// import CourseView from "./pages/CourseView";
// import {
//   BrowserRouter as Router,
//   Route,
//   Routes,
//   useNavigate,
// } from "react-router-dom"; //npm install react-router-dom

// import { Container } from "react-bootstrap";
// import { useEffect, useState } from "react";
// import { UserProvider } from "./UserContext";

// // HOOK [], PROP {}

// // Common patterns in React for a component to return multiple elements
// function App() {
//   // State hook for the user state that's defined here
//   // This will be used to store the user information and will be used for validating if a user is logged in the app or not
//   const [user, setUser] = useState({
//     id: null,
//     isAdmin: null,
//     // email: localStorage.getItem("email")
//   });
//   // This function is for clearing localStorage on logout
//   const unsetUser = () => {
//     localStorage.clear();
//   };

//   // Use to check if the  user information is properly stored upon login and the localStorage is cleared upon logout
//   useEffect(() => {
//     console.log(user);
//     console.log(localStorage);
//   }, [user]);

//   return (
//     // Initializes the dynamic routing will be involve
//     // fragment para sa errors <>

//     <>
//       {/* "UserProvider" component from "UserContext" and wrap all components in the app within it to allow re-rendering when the "value" prop changes. */}
//       {/* Storing information in a context object is done by providing the information using the corresponding "Provider" component and passing the information via the "value" prop.
//       All information provided to the Provider component can be accessed later on from the context object as properties. */}
//       <UserProvider value={{ user, setUser, unsetUser }}>
//         <Router>
//           <AppNavbar />
//           <Container>
//             <Routes>
//               {/* <Route/> - self closing */}
//               <Route path="/" element={<Home />} />
//               <Route path="/courses" element={<Courses />} />
//               <Route path="/courseView" element={<CourseView />} />
//               <Route path="/login" element={<Login />} />
//               <Route path="/register" element={<Register />} />
//               <Route path="/logout" element={<Logout />} />
//               <Route path="/*" element={<PageNotFound />} />
//             </Routes>
//           </Container>
//         </Router>
//       </UserProvider>
//     </>
//   );
// }

// export default App;

import "./App.css";
import { useState, useEffect } from "react";
import { UserProvider } from "./UserContext";
import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import Courses from "./pages/Courses";
import CourseView from "./components/CourseView";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import ErrorPage from "./pages/ErrorPage";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

function App() {
  const [user, setUser] = useState({
    id: null, // dapat nka null, para di always nka login, or png reset
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear(); // png clear, once m logout
  };

  //Because our user state's values are reset to null every time the user reloads the page (thus logging the user out), we want to use React's useEffect hook to fetch the logged-in user's details when the page is reloaded. By using the token saved in localStorage when a user logs in, we can fetch the their data from the database, and re-set the user state values back to the user's details.
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // Set the user states values with the user details upon successful login.
        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });

          // Else set the user states to the initial values
        } else {
          setUser({
            id: null,
            isAdmin: null,
          });
        }
      });
  }, []); // [] to check na dumoble, kung double add ,[]

  return (
    <>
      {/*Provides the user context throughout any component inside of it.*/}
      <UserProvider value={{ user, setUser, unsetUser }}>
        {/*Initializes that dynamic routing will be involved*/}
        <Router>
          <AppNavbar />
          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/courses" element={<Courses />} />
              <Route path="/courses/:courseId" element={<CourseView />} />
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="*" element={<ErrorPage />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
