// import { Nav } from "react-bootstrap";
// import { Navbar } from "react-bootstrap";
// // import { NavDropdown } from "react-bootstrap";
// import { Link, NavLink } from "react-router-dom";
// import { useContext } from "react";
// import UserContext from "../UserContext";

// export default function AppNavbar() {
//   // State to store the user information stored in the login page, or store muna data sa local
//   // The getItem() method - when passed a key name, will return that key's value, or null if the key does not exist, in the given Storage object.
//   // const [user, setUser] = useState(localStorage.getItem("email")); // getting a value local storage
//   const { user } = useContext(UserContext);

//   return (
//     <>
//       <Navbar bg="light" expand="lg">
//         <Navbar.Brand as={Link} href="#home">
//           {/* dito mg pasok ng link, same sa hyperlink */}
//           Zuitt
//         </Navbar.Brand>
//         <Navbar.Toggle aria-controls="basic-navbar-nav" />
//         <Navbar.Collapse id="basic-navbar-nav">
//           <Nav className="ml-auto">
//             <Nav.Link as={NavLink} to="/">
//               Home
//             </Nav.Link>
//             <Nav.Link as={NavLink} to="/courses">
//               Courses
//             </Nav.Link>
//             {user.id !== null ? (
//               //true
//               <Nav.Link as={NavLink} to="/logout">
//                 Logout
//               </Nav.Link>
//             ) : (
//               //false
//               <>
//                 <Nav.Link as={NavLink} to="/register">
//                   Register
//                 </Nav.Link>
//                 <Nav.Link as={NavLink} to="/login">
//                   Login
//                 </Nav.Link>
//               </>
//             )}
//           </Nav>
//         </Navbar.Collapse>
//       </Navbar>
//     </>
//   );
// }

import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { Link, NavLink } from "react-router-dom";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand as={Link} to="/">
        Zuitt
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          <Nav.Link as={NavLink} to="/">
            Home
          </Nav.Link>
          <Nav.Link as={NavLink} to="/courses">
            Courses
          </Nav.Link>
          {user.id ? (
            <Nav.Link as={NavLink} to="/logout">
              Logout
            </Nav.Link>
          ) : (
            <>
              <Nav.Link as={NavLink} to="/login">
                Login
              </Nav.Link>
              <Nav.Link as={NavLink} to="/register">
                Register
              </Nav.Link>
            </>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
