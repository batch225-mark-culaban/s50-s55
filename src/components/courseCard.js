// import { Card } from "react-bootstrap";
// import { Link } from "react-router-dom";
// import { useState, useEffect } from "react";
// import Swal from "sweetalert2";

// // export default function CourseCard() {
// //   return (
// //     <Row className="mt-3 mb-3">
// //       <Col>
// //         <Card className="cardHighlight p-3">
// //           <Card.Body>
// //             <Card.Title>
// //               <h4>Sample Course</h4>
// //             </Card.Title>
// //             <Card.Text>
// //               <h6>Description: </h6>
// //               <p>This is a sample course offering:</p>
// //               <h6>Price:</h6>
// //               <p>Php 40,000</p>
// //             </Card.Text>
// //             <Button variant="primary">Enroll</Button>
// //           </Card.Body>
// //         </Card>
// //       </Col>
// //     </Row>
// //   );
// // }
// // ANSWER;
// export default function CourseCard(course) {
//   // Checks to see if the data was successfully passed
//   console.log(course);
//   // Every component recieves information in a form of an object
//   // console.log(typeof props);

//   const { name, description, price } = course.courseProp;
//   // Use the state hook for this component to be able to store its state
//   // States are used to keep track of the information related to individual components
//   /*
//   SYNTAX

//   const [ getter, setter] = useState(InitialGetterValue)
//   */
//   // const [count, setCount] = useState(0);
//   // 0 for initial state, ex useState(5), mg start sya ng 5

//   const [seats, setSeats] = useState(10);
//   const [isOpen, setIsOpen] = useState(true);

//   // function enroll() {
//   //   // if (count === 30) {
//   //   //   alert("No more seats available");
//   //   //   return;
//   //   // }
//   //   // setCount(count + 1);
//   //   // console.log("enrollees:" + count);
//   //   setSeats(seats - 1);
//   // }

//   // useEffect(() => {
//   //   if (seats === 0) {
//   //     setIsOpen(false);
//   //     Swal.fire("No more slots");
//   //     document.querySelector("#button").setAttribute("disabled", true);
//   //   }
//   // }, [seats]);

//   return (
//     <Card>
//       <Card.Body>
//         <Card.Title>{name}</Card.Title>
//         <Card.Subtitle>Description:</Card.Subtitle>
//         <Card.Text>{description}</Card.Text>
//         <Card.Subtitle>Price:</Card.Subtitle>
//         <Card.Text> Php {price}</Card.Text>
//         {/* <Card.Text>{count} Enrollees</Card.Text> */}
//         {/* <Card.Text>Slot : {seats}</Card.Text> */}
//         <Link className="btn btn-primary" to="/courseView">
//           Details
//         </Link>
//       </Card.Body>
//     </Card>
//   );
// }

import { Card, CardImg } from "react-bootstrap";
import { Link } from "react-router-dom";
// import PropTypes from 'prop-types'

export default function CourseCard(course) {
  // Destructuring the props
  const { name, description, price, image, _id } = course.courseProp;

  // Initialize a 'count' state with a value of zero (0)
  // const [count, setCount] = useState(0)
  // const [slots, setSlots] = useState(15)
  // const [isOpen, setIsOpen] = useState(true)

  // console.log({name})
  // console.log({description})
  // console.log({_id})

  // function enroll(){
  //  if(slots > 0){
  //    setCount(count + 1)
  //    setSlots(slots - 1)

  //    return
  //  }

  //  alert('Slots are full!')
  // }

  // Effects in React is just like side effects/effects in real life, where everytime something happens within the component, a function or condition runs.
  // You may also listen or watch a specific state for changes instead of watching/listening to the whole component
  // useEffect(() => {
  //  if(slots === 0){
  //    setIsOpen(false)
  //  }
  // }, [slots])

  return (
    <>
      <h1>{name}</h1>
      <Card>
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>PHP {price}</Card.Text>
          <Card.Subtitle>Image</Card.Subtitle>
          <CardImg top src={image} />
          <Link className="btn btn-primary" to={`/courses/${_id}`}>
            Details
          </Link>
        </Card.Body>
      </Card>
    </>
  );
}

// Prop Types can be used to validate the data coming from the props. You can define each property of the prop and assign specific validation for each of them.
// {CourseCard.propTypes = {
//   course: PropTypes.shape({
//     name: PropTypes.string.isRequired,
//     description: PropTypes.string.isRequired,
//     price: PropTypes.number.isRequired
//   })
// }}
