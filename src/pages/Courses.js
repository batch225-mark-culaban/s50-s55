// import { useEffect, useState } from "react";
// import CourseCard from "../components/CourseCard";
// import coursesData from "../data/CoursesData";

// export default function Courses() {
//   // Checks to see if mock data was captured

//   // console.log(coursesData);
//   // console.log(coursesData[0]);

//   // The curly braces ({}) are use for props to signify that we are providing information using Javascipt expression
//   //  We can pass information from one component to another using props. This is reffered to as "props drilling"
//   // The 'course' in the CourseCard component is called a "prop" which is a shorthand property and for "property" since components are considered as objects in React JS.

//   // The map method loops to the individual course objects in our array and returns a component for each course
//   // Multiple components created through the map method must have a unique key that will help React JS identify which components/elements have been change, added or removed
//   // Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our courseData array using the courseProp

//   // State that will be used to store the courses retrieved from the database
//   const [courses, setCourses] = [];

//   // Retrieves the courses from the database upon initial render of the "Courses" components.

//   // useEffect(() => {
//   //   fetch("http://localhost:3011/courses/all")
//   //     .then((res) => res.json())
//   //     .then((data) => {
//   //       console.log(data);
//   //     });
//   // });
//   // const courses = coursesData.map((course) => {
//   //   return <CourseCard key={coursesData.id} courseProp={course} />;
//   // });

//   useEffect(() => {
//     fetch(`http://localhost:3011/courses/all`)
//       .then((res) => res.json())
//       .then((data) => {
//         console.log(data);

//         // Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" components
//         setCourses(
//           data.map((course) => {
//             return <CourseCard key={course._id} courseProp={course} />;
//           })
//         );
//       });
//   }, []);

//   return (
//     <>
//       <h1>Courses</h1>
//       {courses}
//     </>
//   );
// }

import CourseCard from "../components/CourseCard";
import Loading from "../components/Loading";
import { useEffect, useState } from "react";

export default function Courses() {
  const [courses, setCourses] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect((isLoading) => {
    // If it detects the data coming from fetch, the set isloading going to be false
    fetch(`${process.env.REACT_APP_API_URL}/courses/`)
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        setCourses(
          result.map((course) => {
            return <CourseCard key={course._id} courseProp={course} />;
          })
        );

        // Sets the loading state to false
        setIsLoading(false);
      });
  }, []);

  return isLoading ? <Loading /> : <>{courses}</>;
}
