// import { Form, Button } from "react-bootstrap";
// import { useEffect, useState, useContext } from "react";
// import UserContext from "../UserContext";
// import { Navigate } from "react-router-dom";
// import Swal from "sweetalert2";

// export default function Register() {
//   const { user } = useContext(UserContext);
//   const [email, setEmail] = useState("");
//   const [password1, setPassword1] = useState("");
//   const [password2, setPassword2] = useState("");
//   // State to determine whether submit button is enabled or not
//   const [isActive, setIsActive] = useState(false); // false = first state is disabled the button

//   useEffect(() => {
//     // Validation to enable submit a button when all fields are populated and both passwords match
//     // State hooks to store the values of he input fields
//     if (
//       email !== "" &&
//       password1 !== "" &&
//       password2 !== "" &&
//       password1 === password2
//     ) {
//       setIsActive(true);
//     } else {
//       setIsActive(false);
//     }
//   }, [email, password1, password2]);

//   function registerUser(e) {
//     e.preventDefault();
//     setEmail("");
//     setPassword1("");
//     setPassword2("");
//     Swal.fire("Registered Successfully!!");
//   }

//   return user.id !== null ? (
//     <Navigate to="/courses" />
//   ) : (
//     <Form onSubmit={(e) => registerUser(e)}>
//       <Form.Group controlId="userEmail">
//         <Form.Label>Email address</Form.Label>
//         <Form.Control
//           type="email"
//           placeholder="Enter email"
//           value={email}
//           onChange={(e) => setEmail(e.target.value)} // target value = input
//           required
//         />
//         <Form.Text className="text-muted">
//           We'll never share your email with anyone else.
//         </Form.Text>
//       </Form.Group>
//       <Form.Group controlId="password1">
//         <Form.Label>Password</Form.Label>
//         <Form.Control
//           type="password"
//           placeholder="Password"
//           value={password1}
//           onChange={(e) => setPassword1(e.target.value)}
//           required
//         />
//       </Form.Group>
//       <Form.Group controlId="password2">
//         <Form.Label>Verify Password</Form.Label>
//         <Form.Control
//           type="password"
//           placeholder="Verify Password"
//           value={password2}
//           onChange={(e) => setPassword2(e.target.value)}
//           required
//         />
//       </Form.Group>
//       {isActive ? (
//         <Button variant="primary" type="submit" id="submitBtn">
//           Submit
//         </Button>
//       ) : (
//         <Button variant="primary" type="submit" id="submitBtn" disabled>
//           Submit
//         </Button>
//       )}
//     </Form>
//   );
// }

// {
//   /* <Button
//           variant="primary"
//           type="submit"
//           id="submitBtn"
//           onSubmit={registerUser}
//           disabled={!isActive}
//         >
//  */
// }
// {
//   /* <Button
// //         variant="primary"
// //         type="submit"
// //         id="submitBtn"
// //         disabled={!(email && password1 && password2 && password1 === password2)}
// //       > */
// }

import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { useNavigate, Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Register() {
  // Activity
  const { user, setUser } = useContext(UserContext); // responsible to get, login, logout, usercontext
  const navigate = useNavigate();
  // Activity END

  const [firstName, setFirstName] = useState(""); // default na blank "", useState == initial state or behavior
  const [lastName, setLastName] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");

  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");

  // For determining if button is disabled or not
  const [isActive, setIsActive] = useState(false);

  function registerUser(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result === true) {
          Swal.fire({
            title: "Oops!",
            icon: "error",
            text: "Email already exist!",
          });
        } else {
          // ACTIVITY HERE
          fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              // parang pumapasok ng data sa server
              firstName: firstName,
              lastName: lastName,
              mobileNo: mobileNumber,
              email: email,
              password: password1,
            }),
          })
            .then((response) => response.json())
            .then((result) => {
              console.log(result);
              // "" clear values when tapos na
              setEmail("");
              setPassword1("");
              setPassword2("");
              setFirstName("");
              setLastName("");
              setMobileNumber("");

              if (result) {
                Swal.fire({
                  title: "Register Successful!",
                  icon: "success",
                  text: "Salamat sa pag-rehistro!",
                });

                navigate("/login");
              } else {
                Swal.fire({
                  title: "Registration Failed",
                  icon: "error",
                  text: "Tropa, mukang mali ang iyong inilagay! :(",
                });
              }
            });
        }
      });
  }

  // function authenticate(event){
  // 	event.preventDefault(event)

  // 	localStorage.setItem('email', email)

  // 	setUser({
  // 		email: localStorage.getItem('email')
  // 	})

  // 	setEmail('')

  // 	navigate('/')
  // }

  // ACTIVITY END===============================================

  useEffect(() => {
    //para mg submit button
    if (
      firstName !== "" &&
      lastName !== "" &&
      mobileNumber.length === 11 &&
      email !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      // Enables the submit button if the form data has been verified
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, mobileNumber, email, password1, password2]);

  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Form onSubmit={(event) => registerUser(event)}>
      <Form.Group controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter First Name"
          value={firstName}
          onChange={(event) => setFirstName(event.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Last Name"
          value={lastName}
          onChange={(event) => setLastName(event.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="mobileNumber">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Mobile Number"
          value={mobileNumber}
          onChange={(event) => setMobileNumber(event.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(event) => setEmail(event.target.value)}
          required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password1}
          onChange={(event) => setPassword1(event.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Verify Password"
          value={password2}
          onChange={(event) => setPassword2(event.target.value)}
          required
        />
      </Form.Group>

      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="primary" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}
