// // use {} if react function
// import { Form, Button } from "react-bootstrap";
// import { useEffect, useState, useContext } from "react";
// import { Navigate } from "react-router-dom";

// // if File do not use {}
// import UserContext from "../UserContext";
// import Swal from "sweetalert2";

// export default function Login() {
//   const { user, setUser } = useContext(UserContext);

//   // State hooks to store the values of the input fields
//   const [email, setEmail] = useState("");
//   const [password, setPassword] = useState("");
//   // const navigate = useNavigate();
//   const [isActive, setIsActive] = useState(false);

//   useEffect(() => {
//     if (email !== "" && password !== "") {
//       setIsActive(true);
//     } else {
//       setIsActive(false);
//     }
//   }, [email, password]);

//   function loginUser(e) {
//     e.preventDefault();

//     fetch("http://localhost:3011/users/login", {
//       method: "POST",
//       headers: {
//         "Content-Type": "application/json",
//       },
//       body: JSON.stringify({
//         email: email,
//         password: password,
//       }),
//     })
//       // Process a fetch request to the corresponding backend API
//       // The header information "Content-Type" is used to specify that the information being sent to the backend will be sent in the form of JSON
//       // The fetch request will communicate with our backend application providing it with a stringified JSON
//       // Convert the information retrieved from the backend into a JavaScript object using the ".then(res => res.json())"
//       // Capture the converted data and using the ".then(data => {})"
//       // Syntax
//       // fetch('url', {options})
//       // .then(res => res.json())
//       // .then(data => {})
//       .then((res) => res.json())
//       .then((data) => {
//         console.log(data);
//         if (typeof data.access !== "undefined") {
//           localStorage.setItem("token", data.access);
//           retrieveUserDetails(data.access);
//         }
//       });

//     const retrieveUserDetails = (token) => {
//       // The token will be sent as part of the request's header information
//       // We put "Bearer" in front of the token to follow implementation standards for JWTs
//       // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
//       fetch("http://localhost:3011/users/details", {
//         headers: {
//           Authorization: `Bearer ${token}`,
//         },
//       })
//         .then((res) => res.json())
//         .then((data) => {
//           console.log(data);

//           setUser({
//             id: data._id,
//             isAdmin: data.isAdmin,
//           });
//         });
//     };
//     // setEmail("");
//     // setPassword("");
//     // navigate("/");

//     Swal.fire("You are now login");

//     // Set the email of the authenticated user in the local storage

//     // SYNTAX localStorage.setItem('PropertyName", value)

//     // localStorage.setItem("email", email); // creating value on the local storage pang testing lng

//     // setUser({ email: localStorage.getItem("email") });
//     // Set the global user State to have properties obtained from local storage
//     // alert(`${email} has been verified! Welcome back!`);
//   }

//   return user.id !== null ? (
//     <Navigate to="/courses" />
//   ) : (
//     //loginUser == authenticated
//     <Form onSubmit={(e) => loginUser(e)}>
//       <h1>Login</h1>
//       <br />
//       <Form.Group controlId="userEmail">
//         <Form.Label>Email address</Form.Label>
//         <Form.Control
//           type="email"
//           placeholder="Enter email"
//           value={email}
//           onChange={(e) => setEmail(e.target.value)}
//           required
//         />
//         <Form.Text className="text-muted"></Form.Text>
//       </Form.Group>
//       <br />
//       <Form.Group controlId="password">
//         <Form.Label>Password</Form.Label>
//         <Form.Control
//           type="password"
//           placeholder="Password"
//           value={password}
//           onChange={(e) => setPassword(e.target.value)}
//           required
//         />
//       </Form.Group>
//       <br />
//       {isActive ? (
//         <Button variant="success" type="submit" id="submitBtn">
//           Login
//         </Button>
//       ) : (
//         <Button variant="secondary" type="submit" id="submitBtn" disabled>
//           Login
//         </Button>
//       )}
//     </Form>
//   );
// }

import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { useNavigate, Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Login() {
  // Initializes the use of the properties from the UserProvider in App.js file
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // Initialize useNavigate
  // const navigate = useNavigate()

  // For determining if button is disabled or not
  const [isActive, setIsActive] = useState(false);

  const retrieveUser = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        // Store the user details retrieved from the token into the global user state
        setUser({
          id: result._id,
          isAdmin: result.isAdmin,
        });
      });
  };

  function authenticate(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (typeof result.access !== "undefined") {
          localStorage.setItem("token", result.access);

          retrieveUser(result.access);

          Swal.fire({
            title: "Login Successful!",
            icon: "success",
            text: "Welcome to Zuitt!",
          });
        } else {
          Swal.fire({
            title: "Authentication Failed!",
            icon: "error",
            text: "Invalid Email or password",
          });
        }
      });
  }

  useEffect(() => {
    if (email !== "" && password !== "") {
      // Enables the submit button if the form data has been verified
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Form onSubmit={(event) => authenticate(event)}>
      <Form.Group controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(event) => setEmail(event.target.value)}
          required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password}
          onChange={(event) => setPassword(event.target.value)}
          required
        />
      </Form.Group>

      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="primary" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}
